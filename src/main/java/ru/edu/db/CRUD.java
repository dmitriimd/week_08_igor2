package ru.edu.db;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CRUD {
    /**
     * Запрос к БД на получение всех объявлений.
     */
    public static final String
            SELECT_ALL_SQL = "SELECT * FROM adverts";
    /**
     * Запрос к БД на получение объявлений по ID.
     */
    public static final String
            SELECT_BY_ID = "SELECT * FROM adverts WHERE id=?";
    /**
     * Запрос к БД на вставку объявления.
     */
    public static final String
            INSERT_SQL = "INSERT INTO adverts "
            + "(title, type, text, price, "
            + "publisher, email, phone, picture_url) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    /**
     * Запрос к БД на обновление данных объявлений по ID.
     */
    public static final String
            UPDATE_SQL = "UPDATE adverts SET "
            + "title=?, type=?, text=?, price=?, "
            + "publisher=?, email=?, phone=?, picture_url=? "
            + "WHERE id=?";
    /**
     * Экземпляр соединения с БД.
     */
    private static CRUD instance;
    /**
     * Источник БД.
     */
    private DataSource dataSource;
    /**
     * Для успешного тестирования.
     */
    private static CRUD override = null;

    /**
     * @param data
     */
    public CRUD(final DataSource data) {
        this.dataSource = data;
    }

    /**
     * Синглтон.
     *
     * @return CRUD
     */
    public static CRUD getInstance() {

        synchronized (CRUD.class) {
            if (override != null) {
                return override;
            }
            if (instance == null) {
                try {
                    Context ctx = new InitialContext();
                    Context env = (Context) ctx.lookup("java:/comp/env");
                    DataSource dataSource =
                            (DataSource) env.lookup("jdbc/dbLink");
                    instance = new CRUD(dataSource);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
        return instance;
    }

    /**
     * Формирует весь список объявлений из БД.
     *
     * @return список объявлений
     */
    public List<Advert> getIndex() {
        return query(SELECT_ALL_SQL);
    }

    /**
     * Выполнение запроса к БД.
     *
     * @param sql
     * @param values
     * @return список объявлений
     */
    private List<Advert> query(final String sql, final Object... values) {
        try (PreparedStatement statement =
                     getConnection().prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            List<Advert> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Формирование объявления из ответа БД.
     *
     * @param resultSet
     * @return объявление
     * @throws SQLException
     */
    private synchronized Advert map(final ResultSet resultSet)
            throws SQLException {
        Advert advert = new Advert();
        advert.setId(resultSet.getLong("id"));
        advert.setTitle(resultSet.getString("title"));
        advert.setType(resultSet.getString("type"));
        advert.setPrice(new BigDecimal(resultSet.getString("price")));
        advert.setText(resultSet.getString("text"));
        advert.setPublisher(resultSet.getString("publisher"));
        advert.setEmail(resultSet.getString("email"));
        advert.setPhone(resultSet.getString("phone"));
        advert.setPictureUrl(resultSet.getString("picture_url"));

        return advert;
    }

    /**
     * Получение соединения с БД.
     *
     * @return соединение с БД.
     */
    private Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Возвращает объявление по его ID.
     *
     * @param id
     * @return объявление
     */
    public Advert getById(final String id) {
        List<Advert> adverts = query(SELECT_BY_ID, id);
        if (adverts.isEmpty()) {
            return null;
        }
        return adverts.get(0);
    }

    /**
     * Выполнение запроса к БД.
     *
     * @param sql
     * @param values
     * @return айди объявления
     */
    private Long execute(final String sql, final Object... values) {
        try (PreparedStatement statement =
                     getConnection()
                             .prepareStatement(sql,
                                     Statement.RETURN_GENERATED_KEYS)
        ) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            Long id = null;
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
            return id;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Сохранение объявления в БД.
     *
     * @param advert
     * @return айди объявлений
     */
    public Long save(final Advert advert) {
        Long id;
        if (advert.getId() == null) {
            id = insert(advert);
        } else {
            id = update(advert);
        }
        return id;
    }

    /**
     * Обновление объявления.
     *
     * @param advert
     * @return айди объявления
     */
    private Long update(final Advert advert) {
        Long id = execute(UPDATE_SQL, advert.getTitle(),
                advert.getType(), advert.getText(),
                advert.getPrice(), advert.getPublisher(),
                advert.getEmail(), advert.getPhone(),
                advert.getPictureUrl(), advert.getId());
        return id;
    }

    /**
     * Вставка объявления в базу.
     *
     * @param advert
     * @return айди объявления
     */
    private Long insert(final Advert advert) {
        Long id = execute(INSERT_SQL, advert.getTitle(),
                advert.getType(), advert.getText(),
                advert.getPrice().toPlainString(),
                advert.getPublisher(), advert.getEmail(),
                advert.getPhone(), advert.getPictureUrl());
        return id;
    }

    /**
     * @param overrideValue
     */
    public static void setOverride(final CRUD overrideValue) {
        CRUD.override = overrideValue;
    }

    /**
     * @return overide
     */
    public static CRUD getOverride() {
        return override;
    }
}
