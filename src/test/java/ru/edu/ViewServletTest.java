package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.db.Advert;
import ru.edu.db.CRUD;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class ViewServletTest {
    private final String PAGE = "/WEB-INF/view.jsp";
    private final String ID_PARAM_MISSING = "/index?error=id_param_missing";
    private final String ID_NOT_FOUND = "/index?error=id_not_found";

    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private RequestDispatcher dispatcher = mock(RequestDispatcher.class);

    private DataSource dataSourceMock = mock(DataSource.class);
    private DataSource dataSource;

    private Advert advert = mock(Advert.class);

    private CRUD crud;
    private ViewServlet view;

    @Before
    public void setup() throws NamingException, SQLException {
        MockContext mockContext = new MockContext(dataSourceMock);
        dataSource = mockContext.getDataSource();

        assertEquals(dataSource, dataSourceMock);

        CRUD.setOverride(mock(CRUD.class));
        crud = CRUD.getInstance();
        view = new ViewServlet();

    }

    @Test
    public void testDoGetWithIdNull() throws ServletException, IOException {
        when(request.getParameter("id")).thenReturn(null);

        view.doGet(request, response);

        verify(response, times(1)).sendRedirect(ID_PARAM_MISSING);
    }

    @Test
    public void testDoGetAdvertNull() throws IOException, ServletException {
        String id = "1";
        when(request.getRequestDispatcher(PAGE)).thenReturn(dispatcher);
        when(request.getParameter("id")).thenReturn(id);
        when(crud.getById(id)).thenReturn(null);

        view.doGet(request, response);
        verify(request, times(1)).getParameter("id");
        verify(response, times(1)).sendRedirect(ID_NOT_FOUND);
    }

    @Test
    public void testDoGet() throws IOException, ServletException {
        String id = "1";
        when(request.getParameter("id")).thenReturn(id);
        when(crud.getById(id)).thenReturn(advert);
        when(request.getRequestDispatcher(PAGE)).thenReturn(dispatcher);

        view.doGet(request, response);
        verify(request, times(1)).getParameter("id");
        verify(request, times(1)).setAttribute("advert", advert);
        verify(dispatcher, times(1)).forward(request, response);
    }

}