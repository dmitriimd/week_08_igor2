package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.db.Advert;
import ru.edu.db.CRUD;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class IndexServletTest {
    private final String PAGE = "/WEB-INF/index.jsp";

    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private RequestDispatcher dispatcher = mock(RequestDispatcher.class);

    private DataSource dataSourceMock = mock(DataSource.class);
    private DataSource dataSource = mock(DataSource.class);

    private IndexServlet index;
    private CRUD crud;

    @Before
    public void setup() throws NamingException {
        MockContext mockContext = new MockContext(dataSourceMock);
        dataSource = mockContext.getDataSource();

        assertEquals(dataSource, dataSourceMock);

        CRUD.setOverride(mock(CRUD.class));
        crud = CRUD.getInstance();
        index = new IndexServlet();
    }

    @Test
    public void testDoGetWithoutError() throws ServletException, IOException {
        List<Advert> adverts = new ArrayList<>();

        when(request.getParameter("error")).thenReturn(null);
        when(request.getRequestDispatcher(PAGE)).thenReturn(dispatcher);
        when(crud.getIndex()).thenReturn(adverts);

        index.doGet(request, response);

        verify(request, times(1)).setAttribute("adverts", adverts);
        verify(dispatcher, times(1)).forward(request, response);
    }

    @Test
    public void testDoGetWithError() throws ServletException, IOException {
        List<Advert> adverts = new ArrayList<>();
        when(request.getParameter("error")).thenReturn("error");
        when(request.getRequestDispatcher(PAGE)).thenReturn(dispatcher);
        when(crud.getIndex()).thenReturn(adverts);

        index.doGet(request, response);

        verify(request, times(1)).setAttribute("error", "error");
        verify(request, times(1)).setAttribute("adverts", adverts);
        verify(dispatcher, times(1)).forward(request, response);
    }

}