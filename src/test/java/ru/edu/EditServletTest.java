package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.db.Advert;
import ru.edu.db.CRUD;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

public class EditServletTest {
    private final String PAGE = "/WEB-INF/edit.jsp";
    private final String REDIRECT_OK = "edit?status=ok&id=";
    private final String REDIRECT_VIEW = "view?id=";

    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private RequestDispatcher dispatcher = mock(RequestDispatcher.class);

    private DataSource dataSourceMock = mock(DataSource.class);
    private DataSource dataSource;

    private Advert advert = mock(Advert.class);

    private CRUD crud;
    private EditServlet edit;

    @Before
    public void setup() throws NamingException {
        MockContext mockContext = new MockContext(dataSourceMock);
        dataSource = mockContext.getDataSource();

        CRUD.setOverride(mock(CRUD.class));
        crud = CRUD.getInstance();
        edit = new EditServlet();
    }

    @Test
    public void testDoGet() throws ServletException, IOException {
        String id = "1";
        when(request.getParameter("id")).thenReturn(id);
        when(request.getRequestDispatcher(PAGE)).thenReturn(dispatcher);
        when(crud.getById(id)).thenReturn(advert);

        edit.doGet(request, response);

        verify(request, times(1)).setAttribute("advert", advert);
        verify(dispatcher, times(1)).forward(request, response);
    }

    @Test
    public void testDoPost() throws IOException {
        Map map = new HashMap();
        map.put("id", new String[]{"0"});
        map.put("title", new String[]{"title"});
        map.put("type", new String[]{"type"});
        map.put("text", new String[]{"text"});
        map.put("price", new String[]{"10"});
        map.put("publisher", new String[]{"publisher"});
        map.put("email", new String[]{"email"});
        map.put("phone", new String[]{"phone"});
        map.put("picture_url", new String[]{"picture_url"});

        when(request.getParameterMap()).thenReturn(map);

        edit.doPost(request, response);

        verify(response, times(1)).sendRedirect(REDIRECT_OK + "0");
    }

    @Test
    public void testDoPostWithTitleEmpty() throws IOException {
        Map map = new HashMap();
        map.put("id", new String[]{"0"});
        map.put("title", new String[]{""});
        map.put("type", new String[]{"type"});
        map.put("text", new String[]{"text"});
        map.put("price", new String[]{"10"});
        map.put("publisher", new String[]{"publisher"});
        map.put("email", new String[]{"email"});
        map.put("phone", new String[]{"phone"});
        map.put("picture_url", new String[]{"picture_url"});

        when(request.getParameterMap()).thenReturn(map);

        edit.doPost(request, response);

        verify(response, times(1)).sendRedirect(REDIRECT_VIEW + "0");
    }

    @Test
    public void testDoPostWithTitleNull() throws IOException {
        Map map = new HashMap();
        map.put("id", new String[]{"0"});
        map.put("title", new String[]{null});
        map.put("type", new String[]{"type"});
        map.put("text", new String[]{"text"});
        map.put("price", new String[]{"10"});
        map.put("publisher", new String[]{"publisher"});
        map.put("email", new String[]{"email"});
        map.put("phone", new String[]{"phone"});
        map.put("picture_url", new String[]{"picture_url"});

        when(request.getParameterMap()).thenReturn(map);

        edit.doPost(request, response);

        verify(response, times(1)).sendRedirect(REDIRECT_VIEW + "0");
    }

}